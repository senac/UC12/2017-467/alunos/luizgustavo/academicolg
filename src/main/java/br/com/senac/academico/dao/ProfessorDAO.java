
package br.com.senac.academico.dao;

import br.com.senac.academico.model.Professor;
import java.util.List;
import javax.persistence.Query;


public class ProfessorDAO extends DAO<Professor>{
    
    public ProfessorDAO() {
        super(Professor.class);
    }
    
    
    
    public static void main(String[] args) {
        
        
        Professor professor = new Professor(); 
      
        
        professor.setNome("Daniel");
        professor.setCidade("Serra");
        professor.setEmail("danielscpereira@gmail.com");
        professor.setCpf("06999664");
        professor.setEndereco("Rua j ");
        professor.setNumero(100);
        
        ProfessorDAO dao = new ProfessorDAO() ; 
        dao.save(professor);
        
        
    }

    public List<Professor> findByFiltro(String codigo, String nome){
        this.em = JPAUtil.getEntityManager();
        List<Professor> lista;
        em.getTransaction().begin();
        
        StringBuilder sql = new StringBuilder("from Professor a where 1=1 ");
        
        if (codigo != null && !codigo.isEmpty()) {
            sql.append(" and a.id = :Id ");
        }
        
        if (codigo != null && !nome.isEmpty()) {
            sql.append(" and a.nome like :Nome ");
        }
        
        Query query = em.createQuery(sql.toString());
        
        if (codigo != null && !codigo.isEmpty()){
            query.setParameter("Id",new Long(codigo));
        }
        
        if (nome != null && !nome.isEmpty()) {
            query.setParameter("Nome", "%" + nome + "%");
        }
        
        lista = query.getResultList();
        
        em.getTransaction().commit();
        em.close();
        
        return lista;
    }
    
}
