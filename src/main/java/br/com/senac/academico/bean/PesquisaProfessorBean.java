
package br.com.senac.academico.bean;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import br.com.senac.academico.model.Professor;
import br.com.senac.academico.dao.ProfessorDAO;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import javax.annotation.PostConstruct;


@Named(value = "pesquisaProfessorBean")
@ViewScoped
public class PesquisaProfessorBean extends Bean{
    private Professor profSelec; 
    private List<Professor> lista;
    private ProfessorDAO dao;
    
    private String id;
    private String nome;
    
    public PesquisaProfessorBean(){
        
    }
    
    @PostConstruct
    public void init(){
        try {
            dao = new ProfessorDAO();
            profSelec = new Professor();
            lista = dao.findAll();
        } catch (Exception e) {
            e.printStackTrace();
            this.addMessageErro("Falha para carregar itens");
        }
    }
    
    public void pesquisa(){
        try {
            this.lista = this.dao.findByFiltro(id,nome);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Professor getProfSelec() {
        return profSelec;
    }

    public void setProfSelec(Professor profSelec) {
        this.profSelec = profSelec;
    }

    public List<Professor> getLista() {
        return lista;
    }

    public void setLista(List<Professor> lista) {
        this.lista = lista;
    }

    public ProfessorDAO getDao() {
        return dao;
    }

    public void setDao(ProfessorDAO dao) {
        this.dao = dao;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public void salvar(){
        if(this.profSelec.getId() == 0){
            dao.save(profSelec);
            this.addMessageInfo("Salvo com sucesso");
        }else{
            dao.update(profSelec);
            this.addMessageInfo("Alterado com sucesso");
        }
        this.pesquisa();
    }
    
    
}
